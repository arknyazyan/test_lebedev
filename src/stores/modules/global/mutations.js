import * as mutation_types from './mutation_types'
export default{
	[mutation_types.M_GET_DOGS](state, payload) {
		state.dogs = state.dogs.concat(payload.data.message)
	},
	[mutation_types.M_GET_DOGS_BREEDS](state, payload) {
		state.dogs = payload.data.message
	},
	[mutation_types.M_GET_BREEDS](state, payload) {
		state.breeds = payload
	},
	[mutation_types.M_SAVE_DOGS_FAVORITE](state, payload) {
		state.favorites.push(payload)
	}
}