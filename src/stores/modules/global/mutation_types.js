export const M_GET_DOGS = 'M_GET_DOGS'
export const M_GET_DOGS_BREEDS = 'M_GET_DOGS_BREEDS'
export const M_GET_BREEDS = 'M_GET_BREEDS'
export const M_SAVE_DOGS_FAVORITE = 'M_SAVE_DOGS_FAVORITE'