import Vue from 'vue'

export default (breed) => {
	if(breed) {
		return Vue.Api.Get(`https://dog.ceo/api/breed/${breed}/images`)
	} else {
		return Vue.Api.Get('https://dog.ceo/api/breeds/image/random/20')
	}
}
